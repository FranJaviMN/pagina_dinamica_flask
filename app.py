#En el siguiente programa vamos a realizar las rutas que va a tener nuestra pagina dinamica creada con flask.

from flask import Flask, render_template, abort
from lxml import etree
app=Flask(__name__)

#Vamos a tener la primera ruta en nuestro programa, aqui vamos a tener la pagina principal, en ella mostraremos nuestro nombre
#y una lista de enlaces a las distintas paginas que vamos a tener en este ejercicio.

@app.route('/')
def inicio():
    nombre='Francisco Javier Martín Núñez'
    return render_template('inicio.html', nombre=nombre)

#Hemos creado la segunda posible ruta que tendra nuesta pagina dinamica en la que se calculara una potencia dada la base de la potencia
#y su exponente nos dara el valor.

@app.route('/potencia/<base>/<exponente>')
def potencia(base, exponente):

#Indicamos las distintas posibilidades que puede tener el exponente. Si nos fijamos vemos que en la ruta no especificamos que tenga que ser
#de tipo entero, esto se debe a que si lo ponemos en la ruta solo podremos elegir numeros mayores o iguales a 0 y no podremos usar negativos,
#por lo que le decimos que es de tipo cadena y lo convertimos a entero dentro de la funcion.

    if int(exponente) > 0:
        resultado = int(base)**int(exponente)

    elif int(exponente) == 0:
        resultado = 1

    elif int(exponente) < 0:
        resultado = 1/(int(base)**int(exponente))
    
    else:
        return abort(404)
    
    return render_template('potencias.html', resultado=resultado, base=base, exponente=exponente)


#Ahora vamos a añadir una nueva ruta con una nueva pagina en la que se va a dar una palabra y una letra, y contara la cantidad de veces
#aparece esa letra en la palabra en cuestion.

@app.route('/cuenta/<palabra>/<letra>')
def cuenta(palabra, letra):

#Verificamos que la variable letra tenga un elemento, si lo tiene seguira sin problemas, si fuese distinto a lo anterior devolvera un error 404.

    if len(letra) == 1:
        cantidad =  palabra.count(letra)
        return render_template('contar.html', cantidad=cantidad, palabra=palabra, letra=letra)

    else:
        return abort(404)


#Ahoara vamos a añadir una nueva ruta con una nueva pagina en la que, leyendo un fichero .xml donde tenemos libros, vamos a introducir
#en la ruta un codigo y este lo vamos a usar para poder sacar la biblioteca donde se encuentra, el nombre del autor del libro y el nombre del libro.

@app.route('/libro/<codigo>')
def libros(codigo):
    doc = etree.parse('libros.xml')

    #Verificamos que el codigo que hemos introducido en la ruta esta en nuestro fichero .xml, si esta, selecciona el nombre del autor y
    #el titulo del libro con el codigo, sino esta, devuelve un error 404

    if str(codigo) in doc.xpath("/biblioteca/libro/codigo/text()"):
        titulo=doc.xpath('/biblioteca/libro[codigo="%s"]/titulo/text()' %(codigo))[0]
        nombre_autor=doc.xpath('/biblioteca/libro[codigo="%s"]/autor/text()' %codigo)[0]
        return render_template('libros.html', nombre_libro=titulo, nombre_autor=nombre_autor)
    else:
        return abort(404)


#Si aun estamos en desarrollo de la pagina dinamica tener esta opcion sin comentar.
app.run(debug=True)

#Si ya esta nuestra pagina en produccion, comentar la opcion anterior y descomentar la siguiente.
#app.run()